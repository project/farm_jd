# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## 1.0.0-beta5 2025-02-05

### Fixed

- CRON job errors when module is installed but not configured. [#3493063](https://www.drupal.org/project/farm_jd/issues/3493063)

## 1.0.0-beta4 2024-04-15

### Added

- Add status message to report how many items were imported.
- Add revision info to log imports.

### Changed

- Use standard log directory for file storage.
- Change use farm_jd to import farm_jd permission.
- Only add administer and connect permissions to account admin role.

### Fixed

- Default file storage to private file system.

## 1.0.0-beta3 2024-03-27

### Changed

- Sort quantities to keep target and result sets together.

## 1.0.0-beta2 2024-02-09

### Added

- [Issue #3419620: Use configured units of measure for measurements](https://www.drupal.org/project/farm_jd/issues/3419620)
- [Issue #3419619: Make shapefile syncing configurable via settings](https://www.drupal.org/project/farm_jd/issues/3419619)

### Changed

- Include product names in logs.

## 1.0.0-beta1 2024-02-04

### Added

- [Issue #3414166: Import the seeding product](https://www.drupal.org/project/farm_jd/issues/3414166)
- [Issue #3397731: Add settings option for sandbox or production](https://www.drupal.org/project/farm_jd/issues/3397731)
- [Issue #3395420: Import shape files with field operations](https://www.drupal.org/project/farm_jd/issues/3395420)
- Improve batch import message.

### Fixed

- Using ${var} in strings is deprecated, use {$var} instead.
- [Issue #3419130: Breaking API change: products field](https://www.drupal.org/project/farm_jd/issues/3419130)

## 1.0.0-alpha3 2023-12-01

### Fixed

- Fix infinite loop when refreshing tokens.
- Prevent duplicate field operations from syncing.

## 1.0.0-alpha2 2023-11-22

### Fixed

- Module dependencies do not need farm: prefix.

## 1.0.0-alpha1 2023-11-22

Initial alpha release.

This does not yet include support for production APIs.
