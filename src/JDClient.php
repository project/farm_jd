<?php

namespace Drupal\farm_jd;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\RequestOptions;

/**
 * John Deere client.
 */
class JDClient extends Client implements JDClientInterface {

  /**
   * The John Deere API URL.
   *
   * @var string
   */
  protected $apiUrl;

  /**
   * The John Deere authorization URL.
   *
   * @var string
   */
  protected $oauthWellKnown = 'https://signin.johndeere.com/oauth2/aus78tnlaysMraFhC1t7/.well-known/oauth-authorization-server';

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The JD config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $jdConfig;

  /**
   * The John Deere OAuth token.
   *
   * @var array
   */
  protected $token;

  /**
   * The client ID.
   *
   * @var string|null
   */
  protected $clientId;

  /**
   * The client secret.
   *
   * @var string|null
   */
  protected $clientSecret;

  /**
   * The connected Org ID.
   *
   * @var int|null
   */
  protected $orgId;

  /**
   * The configured units of measure.
   *
   * @var string
   */
  protected $units;

  /**
   * Boolean if shapefiles should be synced.
   *
   * @var bool
   */
  protected $autosyncShapefiles;

  /**
   * John Deere client constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param array $config
   *   Guzzle client config.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StateInterface $state, TimeInterface $time, array $config = []) {
    $this->state = $state;
    $this->time = $time;
    $this->jdConfig = $config_factory->get('farm_jd.settings');

    // Set API URL based on the environment.
    if ($config['environment'] === 'production') {
      $this->apiUrl = 'https://api.deere.com/platform/';
    }
    else {
      $this->apiUrl = 'https://sandboxapi.deere.com/platform/';
    }

    // Save unit of measure.
    $this->units = $config['units'] ?? 'METRIC';
    $this->autosyncShapefiles = $config['autosync_shapefiles'] ?? FALSE;

    $this->clientId = $config['client_id'] ?? NULL;
    $this->clientSecret = $config['client_secret'] ?? NULL;
    $this->orgId = $config['org_id'] ?? NULL;
    $this->token = [];
    $default_config = [
      'base_uri' => $this->apiUrl,
      'http_errors' => FALSE,
    ];
    $config += $default_config;
    parent::__construct($config);
  }

  /**
   * {@inheritdoc}
   */
  public function requestAsync(string $method, $uri = '', array $options = []): PromiseInterface {
    // Build an authorization header for each request refreshing the OAuth
    // token when necessary.
    $default_headers = $this->getAuthorizationHeader();
    $headers = $options['headers'] ?? [];
    $options['headers'] = $headers + $default_headers;
    $options['headers']['Accept'] = 'application/vnd.deere.axiom.v3+json';

    return parent::requestAsync($method, $uri, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function clientIsConfigured(): bool {
    return !empty($this->clientId) && !empty($this->clientSecret);
  }

  /**
   * {@inheritdoc}
   */
  public function organizationIsConfigured(): bool {
    return !empty($this->orgId);
  }

  /**
   * {@inheritdoc}
   */
  public function getAuthUrl() {
    // Create a new Guzzle client.
    $client = new Client(
      [
        'http_errors' => FALSE,
      ]
    );
    $response = $client->request('GET', $this->oauthWellKnown);
    $endpoints = Json::decode($response->getBody());
    return $endpoints['authorization_endpoint'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getTokenUrl() {
    // Create a new Guzzle client.
    $client = new Client(
      [
        'http_errors' => FALSE,
      ]
    );
    $response = $client->request('GET', $this->oauthWellKnown);
    $endpoints = Json::decode($response->getBody());
    return $endpoints['token_endpoint'] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setToken(array $token) {
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public function grant(array $params): array {
    // Create a new Guzzle client.
    $client = new Client(
          [
            'base_uri' => $this->getTokenUrl(),
            'http_errors' => FALSE,
            'headers' => ['accept' => 'application/vnd.deere.axiom.v3+json'],
          ]
      );

    $response = $client->request('POST', '', ['form_params' => $params]);
    $token = Json::decode($response->getBody());

    // Set expiration time.
    $now = $this->time->getCurrentTime();
    $expires_at = $now + $token['expires_in'] ?? 0;
    $token['expires_at'] = $expires_at;

    // Set the token for the client and return.
    $this->setToken($token);
    $this->state->set('farm_jd.token', $token);

    return $token;
  }

  /**
   * {@inheritdoc}
   */
  public function refreshToken(): array {
    // Bail if there is no refresh token.
    if (empty($this->token['refresh_token'])) {
      return [];
    }

    // Build refresh token params.
    $refreshToken = $this->token['refresh_token'];
    $params = [
      'grant_type' => 'refresh_token',
      'client_id' => $this->clientId,
      'client_secret' => $this->clientSecret,
      'refresh_token' => $refreshToken,
    ];

    return $this->grant($params);
  }

  /**
   * Helper function to build the authorization header.
   *
   * @param bool $refresh
   *   Boolean indicating if a refresh can be performed.
   *
   * @return array
   *   An array with the Authorization header
   */
  protected function getAuthorizationHeader(bool $refresh = TRUE): array {
    // Bail if there is no token.
    $header = [];
    if (empty($this->token)) {
      return $header;
    }

    // Refresh the token if expired.
    $now = $this->time->getCurrentTime();
    $expired = ($now + 120) > $this->token['expires_at'] ?? PHP_INT_MAX;
    if ($expired && $refresh) {
      $this->refreshToken();

      return $this->getAuthorizationHeader(FALSE);
    }

    // Return the access token in a Bearer Authorization header.
    $access_token = $this->token['access_token'];
    $header['Authorization'] = "Bearer $access_token";

    return $header;
  }

  /**
   * {@inheritdoc }
   */
  public function getUnits(): ?string {
    return $this->units ?? 'METRIC';
  }

  /**
   * {@inheritdoc}
   */
  public function getAutosyncShapefiles(): bool {
    return $this->autosyncShapefiles;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrganization() {
    // Check for Organization ID.
    if (!$this->organizationIsConfigured()) {
      return NULL;
    }

    // Get the organization data.
    $response = $this->request('GET', "organizations/$this->orgId");

    // Return null on failure.
    if ($response->getStatusCode() != 200) {
      return NULL;
    }

    // Return the organization data.
    $response_body = Json::decode($response->getBody());

    return $response_body ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function requestOrganizationApiList(string $api_type, array $options) {

    // Check if the provided apiType is allowed.
    $allowedApiTypes = [
      'fields',
      'machines',
    ];
    if (!in_array($api_type, $allowedApiTypes)) {
      throw new \InvalidArgumentException("The provided API type '$api_type' is not allowed.");
    }

    // Check for Organization ID.
    if (!$this->organizationIsConfigured()) {
      return NULL;
    }

    // Get the data from the constructed endpoint.
    $options[RequestOptions::HEADERS] = ($options[RequestOptions::HEADERS] ?? []) + ['x-deere-no-paging' => TRUE];
    return $this->request(
      'GET',
      "organizations/$this->orgId/$api_type",
      $options,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getOrganizationApiList(string $api_type, array $options = []) {

    // Return empty list on failure.
    $response = $this->requestOrganizationApiList($api_type, $options);
    if ($response->getStatusCode() != 200) {
      return [];
    }

    // Return the data.
    $response_body = Json::decode($response->getBody());
    return $response_body['values'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getOrganizationApiListChanges(string $api_type, string $signature = '') {

    // Request the organization list with the signature header.
    $options = [
      RequestOptions::HEADERS => [
        'x-deere-signature' => $signature,
      ],
    ];
    $response = $this->requestOrganizationApiList($api_type, $options);

    // Return early if there are no changes.
    if ($response->getStatusCode() == 304) {
      return [
        'values' => [],
        'signature' => $signature,
      ];
    }

    // Bail if failure.
    if ($response->getStatusCode() != 200) {
      return [
        'values' => [],
        'signature' => NULL,
      ];
    }

    // Return the changed values with the new signature.
    $headers = $response->getHeaders();
    $response_body = Json::decode($response->getBody());
    return [
      'values' => $response_body['values'] ?? [],
      'signature' => reset($headers['x-deere-signature']),
    ];
  }

  /**
   * Fetches details of a specific machine based on its ID.
   *
   * @param string $machine_id
   *   The ID of the machine.
   *
   * @return array|null
   *   Returns the decoded response body or NULL on failure.
   */
  public function getJdMachineById(string $machine_id) {

    // Get the machine details from the constructed endpoint.
    $response = $this->request('GET', "machines/{$machine_id}");

    // Return null on failure.
    if ($response->getStatusCode() != 200) {
      return NULL;
    }

    // Return the machine details.
    $response_body = Json::decode($response->getBody());

    return $response_body ?? NULL;
  }

  /**
   * Fetches details of a specific field based on its ID.
   *
   * @param string $field_id
   *   The ID of the field.
   *
   * @return array|null
   *   Returns the decoded response body or NULL on failure.
   */
  public function getJdFieldById(string $field_id) {

    // Check for Organization ID.
    if (!$this->organizationIsConfigured()) {
      return NULL;
    }

    // Get the field details from the constructed endpoint.
    $response = $this->request(
      'GET',
      "organizations/$this->orgId/fields/$field_id",
      [
        RequestOptions::QUERY => ['embed' => 'activeBoundary'],
      ],
    );

    // Return null on failure.
    if ($response->getStatusCode() != 200) {
      return NULL;
    }

    // Return the field details.
    $response_body = Json::decode($response->getBody());

    return $response_body ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldOperationById(string $field_operation_id) {

    // Check for Organization ID.
    if (!$this->organizationIsConfigured()) {
      return NULL;
    }

    // Get the field details from the constructed endpoint.
    $response = $this->request('GET', "fieldOperations/{$field_operation_id}");

    // Return null on failure.
    if ($response->getStatusCode() != 200) {
      return NULL;
    }

    // Return the field details.
    $response_body = Json::decode($response->getBody());

    return $response_body ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function requestFieldFieldOperations(string $field_id, array $options) {
    $options[RequestOptions::HEADERS] = ($options[RequestOptions::HEADERS] ?? []) + ['x-deere-no-paging' => TRUE];
    return $this->request(
      'GET',
      "organizations/$this->orgId/fields/$field_id/fieldOperations",
      $options,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldFieldOperationsChanges(string $field_id, string $signature = '') {

    // Request field operations with the signature header.
    $options = [
      RequestOptions::HEADERS => [
        'x-deere-signature' => $signature,
      ],
    ];
    $response = $this->requestFieldFieldOperations($field_id, $options);

    // Return early if there are no changes.
    if ($response->getStatusCode() == 304) {
      return [
        'values' => [],
        'signature' => $signature,
      ];
    }

    // Bail if failure.
    if ($response->getStatusCode() != 200) {
      return [
        'values' => [],
        'signature' => NULL,
      ];
    }

    // Return the changed values with the new signature.
    $headers = $response->getHeaders();
    $response_body = Json::decode($response->getBody());
    return [
      'values' => $response_body['values'] ?? [],
      'signature' => reset($headers['x-deere-signature']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldOperationShapefile(string $operation_id, string $filename = NULL) {
    $options[RequestOptions::QUERY] = [
      'shapeType' => 'Point',
      'resolution' => 'EachSection',
    ];
    $options[RequestOptions::HEADERS] = [
      'Accept-UOM-System' => $this->getUnits(),
    ];
    $response = $this->request(
      'GET',
      "fieldOps/$operation_id",
      $options,
    );

    // Bail if not a 200 status code.
    if ($response->getStatusCode() !== 200) {
      return NULL;
    }

    return (string) $response->getBody();
  }

  /**
   * {@inheritdoc}
   */
  public function checkFieldOperationShapefile(string $operation_id) {
    $options[RequestOptions::QUERY] = [
      'shapeType' => 'Point',
      'resolution' => 'EachSection',
    ];
    $options[RequestOptions::HEADERS] = [
      'Accept-UOM-System' => $this->units,
    ];
    $response = $this->request(
      'GET',
      "fieldOps/$operation_id",
      $options,
    );
    return $response->getStatusCode();
  }

}
