<?php

namespace Drupal\farm_jd\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * John Deere settings form.
 */
class JDSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'farm_jd_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['farm_jd.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Load the configuration object.
    $config = $this->config('farm_jd.settings');

    // Environment field.
    $form['environment'] = [
      '#type' => 'select',
      '#title' => $this->t('Environment'),
      '#options' => [
        'sandbox' => $this->t('Sandbox'),
        'production' => $this->t('Production'),
      ],
      '#default_value' => $config->get('environment') ?: 'sandbox',
      '#required' => TRUE,
    ];

    // Client ID field.
    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
      '#required' => TRUE,
    ];

    // Secret ID field.
    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('client_secret'),
      '#required' => TRUE,
    ];

    $form['autosync_shapefiles'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Autosync Shapefiles'),
      '#description' => $this->t('Shapefiles can be imported with every Field Operation but can take up lots of storage space (~1MB to 10sMB each). Only check this if your farmOS instance has sufficient storage.'),
      '#default_value' => $config->get('autosync_shapefiles'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get the submitted form values.
    $values = $form_state->getValues();

    // Save the form values to the configuration.
    $this->config('farm_jd.settings')
      ->set('client_id', $values['client_id'])
      ->set('client_secret', $values['client_secret'])
      ->set('environment', $values['environment'])
      ->set('autosync_shapefiles', $values['autosync_shapefiles'])
      ->save();

    $this->getLogger('farm_jd')->info('Updated John Deere client settings.');

    parent::submitForm($form, $form_state);
  }

}
