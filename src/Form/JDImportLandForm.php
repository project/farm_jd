<?php

namespace Drupal\farm_jd\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\farm_jd\Event\JDEvent;

/**
 * Provides a form for importing John Deere land data.
 */
class JDImportLandForm extends JDBaseImportForm {

  /**
   * {@inheritdoc}
   */
  protected string $dataType = JDEvent::FIELD;

  /**
   * {@inheritdoc}
   */
  protected function getDataType() {
    return 'fields';
  }

  /**
   * Define headers for the table.
   *
   * @return array
   *   Array of header labels indexed by key.
   */
  protected function getHeaders() {
    return [
      'name' => $this->t('Name'),
      'jd_id' => $this->t('John Deere ID'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Fetch paginated land data.
    $page_size = $this->getPageSize($form_state);
    $search_term = $this->getSearchTerm($form_state);

    $changed_fields = $this->getPaginatedData('farm_jd_updates_fields', ['jd_id', 'name', 'modified'], 'name', $page_size, $search_term);
    $form['land'] = $this->createDataTable($changed_fields, 'Land');

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import Selected Land'),
      '#disabled' => count($changed_fields) == 0,
      '#attributes' => [
        'class' => ['button--primary'],
      ],
    ];

    $form['refresh'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh'),
      '#submit' => ['::processFieldUpdates'],
    ];

    return $form;
  }

  /**
   * Helper function to process changed fields.
   *
   * @return array
   *   The changed fields.
   */
  public function processFieldUpdates(): array {
    $changes = $this->jdSync->checkFieldUpdates();
    $count = count($changes['values']);
    $this->messenger()->addMessage("Found $count land changes.");
    return $changes;
  }

}
