<?php

namespace Drupal\farm_jd\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\PagerSelectExtender;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\BaseFormIdInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Render\Element\Checkboxes;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\farm_jd\Event\JDEvent;
use Drupal\farm_jd\JDClientInterface;
use Drupal\farm_jd\JDStatusTrait;
use Drupal\farm_jd\JDSyncInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for John Deere data imports.
 */
abstract class JDBaseImportForm extends FormBase implements BaseFormIdInterface, ContainerInjectionInterface {

  use LoggerChannelTrait;
  use JDStatusTrait;

  /**
   * The JD data type.
   *
   * @var string
   */
  protected string $dataType;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManger;

  /**
   * The John Deere client service.
   *
   * @var \Drupal\farm_jd\JDClientInterface
   */
  protected $jdClient;

  /**
   * The JD Sync service.
   *
   * @var \Drupal\farm_jd\JDSyncInterface
   */
  protected $jdSync;

  /**
   * Constructs a JDBaseImportForm object.
   */
  public function __construct(Connection $database, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, JDClientInterface $jdClient, JDSyncInterface $jdSync) {
    $this->database = $database;
    $this->state = $state;
    $this->entityTypeManger = $entity_type_manager;
    $this->jdClient = $jdClient;
    $this->jdSync = $jdSync;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('state'),
      $container->get('entity_type.manager'),
      $container->get('farm_jd.jd_client'),
      $container->get('farm_jd.jd_sync'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return 'jd_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "jd_import_{$this->dataType}_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Check JD status.
    $this->checkJdStatus(TRUE);

    $form['#attached']['library'][] = 'farm_jd/inline-container';

    $form['inline_container'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['inline-container']],
    ];

    // Get the search term from the user input.
    $search = $this->getSearchTerm($form_state);

    $form['inline_container']['search'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Search'),
      '#default_value' => $search,
      '#placeholder' => $this->t('Search by fields...'),
    ];

    // Get the current page size.
    $page_size = $this->getPageSize($form_state);

    $form['inline_container']['page_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Page Size'),
      '#options' => [
        '50' => $this->t('50'),
        '100' => $this->t('100'),
        '250' => $this->t('250'),
        '500' => $this->t('500'),
      ],
      '#default_value' => $page_size,
    ];

    $form['inline_container']['apply'] = [
      '#type' => 'submit',
      '#value' => $this->t('Apply'),
      '#submit' => ['::applySubmit'],
    ];

    // Set up the pager.
    $form['pager'] = [
      '#type' => 'pager',
      '#parameters' => [
        'page_size' => $page_size,
      ],
      '#weight' => 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $operations = [];
    foreach (Checkboxes::getCheckedCheckboxes($form_state->getValue('table')) as $id) {
      $operations[] = [
        [JDBaseImportForm::class, 'importItemBatch'],
        [$this->dataType, $id, JDEvent::ACTION_IMPORTED, []],
      ];
    }
    $batch = [
      'operations' => $operations,
      'title' => $this->t('Importing John Deere data'),
      'error_message' => $this->t('Error importing John Deere data.'),
      'finished' => [JDBaseImportForm::class, 'batchFinished'],
    ];
    batch_set($batch);
  }

  /**
   * Return the data type for the import. Must be implemented by subclasses.
   */
  abstract protected function getDataType();

  /**
   * Prepares options for the form table.
   */
  protected function prepareOptions(array $keys, array $items) {
    $options = [];
    foreach ($items as $item) {
      $id = $item['jd_id'];
      foreach ($keys as $key) {
        $value = $item[$key] ?? '';
        if (is_array($item[$key])) {
          $value = $item[$key]['name'] ?? '';
        }

        // Convert dates.
        if (in_array($key, ['start', 'end', 'modified'])) {
          $value = date('Y-m-d', $value);
        }

        $options[$id][$key] = $value;
      }
    }
    return $options;
  }

  /**
   * Creates a data table for the form.
   */
  protected function createDataTable(array $items, string $title) {
    $headers = $this->getHeaders();

    // Check the last sync time.
    $time_ago = $this->t('Never');
    if ($last_sync = $this->jdSync->getApiLastUpdate($this->getDataType())) {
      /** @var \Drupal\Core\Datetime\DateFormatterInterface $formatter */
      // phpcs:ignore
      $formatter = \Drupal::service('date.formatter');
      $time_ago = $formatter->formatTimeDiffSince($last_sync);
    }

    $empty = "No $title for import. Last sync: $time_ago";
    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $headers,
      '#empty' => $empty,
      '#options' => $this->prepareOptions(array_keys($headers), $items),
    ];
    return $form;
  }

  /**
   * Define headers for the table.
   *
   * @return array
   *   Array of header labels indexed by key.
   */
  protected function getHeaders() {
    return [
      'name' => $this->t('Name'),
    ];
  }

  /**
   * Implements callback_batch_operation().
   *
   * Dispatches events to import JD data.
   *
   * @param string $data_type
   *   The JD data type.
   * @param string $id
   *   The JD data ID.
   * @param string $action
   *   The event ID.
   * @param array $data
   *   The event data.
   * @param array $context
   *   The batch context.
   */
  public static function importItemBatch(string $data_type, string $id, string $action, array $data, array &$context) {
    $event = new JDEvent($id, $action, $data);
    \Drupal::service('event_dispatcher')->dispatch($event, $data_type);
    $context['results'][] = $id;

    // Build status message.
    $label = $id;
    if (count($event->assets)) {
      $asset = reset($event->assets);
      $label = $asset->label();
    }
    if (count($event->logs)) {
      $log = reset($event->logs);
      $label = $log->label();
    }

    $context['message'] = t('Imported %label', ['%label' => $label]);
  }

  public static function batchFinished($success, $results, $operations) {
    if ($success) {
      $message = new PluralTranslatableMarkup(
        count($results),
        'Imported @count item',
        'Imported @count items',
        [],
        [],
        \Drupal::service('string_translation'),
      );
      \Drupal::messenger()->addMessage($message);
    }
    else {
      // An error occurred.
      $message = new PluralTranslatableMarkup(
        count($operations),
        'Error importing @count item',
        'Error importing @count items',
        [],
        [],
        \Drupal::service('string_translation'),
      );
      \Drupal::messenger()->addWarning($message);
    }
  }

  /**
   * Fetches paginated data from the database.
   *
   * This method is used to retrieve a subset of data from a database table,
   * applying pagination based on the current page and the provided page size.
   *
   * @param string $table
   *   The database table from which to fetch the data.
   * @param array $fields
   *   An array of field names to select.
   * @param string $order_by
   *   The field by which to order the results.
   * @param int $page_size
   *   The number of records to retrieve per page. Defaults to 50.
   * @param string $search
   *   A search term for filtering by 'name' or 'jd_id'. Default is empty.
   *
   * @return array
   *   An array of database records for the current page.
   */
  protected function getPaginatedData($table, $fields, $order_by, $page_size = 50, $search = '') {
    $query = $this->database->select($table, 't')
      ->fields('t', $fields)
      ->orderBy($order_by)
      ->extend(PagerSelectExtender::class)
      ->limit($page_size);

    // If the search parameter is set, add a condition to the query.
    if (!empty($search) && !empty($fields)) {
      $or = $query->orConditionGroup();
      foreach ($fields as $field) {
        $or->condition('t.' . $field, '%' . $this->database->escapeLike($search) . '%', 'LIKE');
      }
      $query->condition($or);
    }

    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);

    return $result;
  }

  /**
   * Helper function to determine the page size.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return int
   *   The page size.
   */
  protected function getPageSize(FormStateInterface $form_state) {
    return $this->getRequest()->query->get('page_size') ?? $form_state->getValue('page_size', 50);
  }

  /**
   * Helper function to determine the search term.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return string
   *   The search term.
   */
  protected function getSearchTerm(FormStateInterface $form_state) {
    return $this->getRequest()->query->get('search') ?? $form_state->getValue('search', '');
  }

  /**
   * Custom submit handler for the apply button.
   */
  public function applySubmit(array &$form, FormStateInterface $form_state) {

    // Reload the form with the desired page size.
    $input = $form_state->getUserInput();
    $route = $this->getRouteMatch()->getRouteName();

    // Prepare the query parameters including the page size and search term.
    $query_params = [
      'page_size' => $input['page_size'],
    ];

    // Include the search term in the query parameters if it is set.
    if (!empty($input['search'])) {
      $query_params['search'] = $input['search'];
    }

    // Redirect to the same route with the new query parameters.
    $form_state->setRedirect($route, [], ['query' => $query_params]);
  }

}
