<?php

namespace Drupal\farm_jd\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the John Deere Organization form.
 */
class JDOrganizationForm extends FormBase {

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a JDOrganizationForm object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'farm_jd_organization';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['message'] = [
      '#markup' => $this->t('Please select the organization you would like to connect with John Deere.'),
    ];

    // Fetching organization options from state.
    $org_options = $this->state->get('farm_jd.org_options', []);

    $form['select_organization'] = [
      '#type' => 'select',
      '#title' => $this->t('Organization'),
      '#options' => $org_options,
      '#default_value' => !empty($org_options) ? reset($org_options) : NULL,
      '#required' => TRUE,
      '#description' => $this->t('Select the John Deere Organization you wish to connect with.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Connect'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selectedOrg = $form_state->getValue('select_organization');

    // Checking the selected organization and setting it.
    if (!empty($selectedOrg)) {
      $this->state->set('farm_jd.user_id', $this->currentUser()->id());
      $this->state->set('farm_jd.org_id', $selectedOrg);
      // Redirecting user to the status page after form submission.
      $form_state->setRedirect('farm_jd.status');
    }
    $this->messenger()->addStatus($this->t('Connected to organization.'));
    $this->getLogger('farm_jd')->info("Organization set to: $selectedOrg.");
  }

}
