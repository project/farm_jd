<?php

namespace Drupal\farm_jd\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\farm_jd\Event\JDEvent;

/**
 * Provides a form for importing John Deere land data.
 */
class JDImportOperationForm extends JDBaseImportForm {

  /**
   * {@inheritdoc}
   */
  protected string $dataType = JDEvent::FIELD_OPERATION;

  /**
   * {@inheritdoc}
   */
  protected function getDataType() {
    return 'field_operations';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Fetch paginated operation data.
    $page_size = $this->getPageSize($form_state);
    $search_term = $this->getSearchTerm($form_state);

    $changed_operations = $this->getPaginatedData('farm_jd_updates_field_operations', ['jd_id', 'field_name', 'operation_type', 'season', 'product', 'start', 'end', 'modified'], 'start', $page_size, $search_term);
    $form['operations'] = $this->createDataTable($changed_operations, 'Field Operations');

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import Selected Field Operations'),
      '#disabled' => count($changed_operations) == 0,
      '#attributes' => [
        'class' => ['button--primary'],
      ],
    ];

    $form['refresh'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh'),
      '#submit' => ['::processFieldOperationUpdates'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getHeaders() {
    return [
      'field_name' => $this->t('Field'),
      'operation_type' => $this->t('Operation Type'),
      'season' => $this->t('Crop Season'),
      'product' => $this->t('Product'),
      'start' => $this->t('Start Date'),
      'jd_id' => $this->t('John Deere ID'),
    ];
  }

  /**
   * Helper function process changed field operations.
   *
   * @return array
   *   The changed field operations.
   */
  public function processFieldOperationUpdates() {
    $changes = $this->jdSync->checkFieldOperationUpdates();
    $count = count($changes['values']);
    $this->messenger()->addMessage("Found $count field operation changes.");
    return $changes;
  }

}
