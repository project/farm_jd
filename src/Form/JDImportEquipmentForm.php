<?php

namespace Drupal\farm_jd\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\farm_jd\Event\JDEvent;

/**
 * Provides a form for importing John Deere equipment data.
 */
class JDImportEquipmentForm extends JDBaseImportForm {

  /**
   * {@inheritdoc}
   */
  public string $dataType = JDEvent::MACHINE;

  /**
   * Implement the abstract method from the base class.
   */
  protected function getDataType() {
    return 'machines';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Fetch paginated equipment data.
    $page_size = $this->getPageSize($form_state);
    $search_term = $this->getSearchTerm($form_state);

    $changed_equipment = $this->getPaginatedData('farm_jd_updates_machines', ['jd_id', 'name', 'make', 'model'], 'name', $page_size, $search_term);
    $form['equipment'] = $this->createDataTable($changed_equipment, 'Equipment');

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import Selected Equipment'),
      '#disabled' => count($changed_equipment) == 0,
      '#attributes' => [
        'class' => ['button--primary'],
      ],
    ];

    $form['refresh'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh'),
      '#submit' => ['::processMachineUpdates'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getHeaders() {
    return parent::getHeaders() + [
      'make' => $this->t('Make'),
      'model' => $this->t('Model'),
      'jd_id' => $this->t('John Deere ID'),
    ];
  }

  /**
   * Helper function to process changed machines.
   *
   * @return array
   *   The changed machines.
   */
  public function processMachineUpdates(): array {
    $changes = $this->jdSync->checkEquipmentUpdates();
    $count = count($changes['values']);
    $this->messenger()->addMessage("Found $count equipment changes.");
    return $changes;
  }

}
