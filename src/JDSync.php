<?php

namespace Drupal\farm_jd;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;

/**
 * Service for synchronizing data with JD.
 */
class JDSync implements JDSyncInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The JD Client service.
   *
   * @var \Drupal\farm_jd\JDClientInterface
   */
  protected $jdClient;

  /**
   * Constructor for the JDClientFactory.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\farm_jd\JDClientInterface $jd_client
   *   The JD Client service.
   */
  public function __construct(Connection $database, StateInterface $state, EntityTypeManagerInterface $entity_type_manager, TimeInterface $time, JDClientInterface $jd_client) {
    $this->database = $database;
    $this->state = $state;
    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
    $this->jdClient = $jd_client;
  }

  /**
   * {@inheritdoc}
   */
  public function resetApiUpdates(string $api_type) {
    $this->state->delete("farm_jd.etag.$api_type");
    $this->state->delete("farm_jd.etag.$api_type.last_updated");
  }

  /**
   * {@inheritdoc}
   */
  public function getApiLastUpdate(string $api_type): int|NULL {
    return $this->state->get("farm_jd.etag.$api_type.last_updated", NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function checkEquipmentUpdates(bool $force = TRUE): array {
    $api_type = 'machines';

    // Bail if organization is not configured.
    if (!$this->jdClient->organizationIsConfigured()) {
      return [
        'values' => [],
      ];
    }

    // Check last update.
    $last = $this->getApiLastUpdate($api_type);
    if (!$force && ($last = $this->getApiLastUpdate($api_type)) && $this->time->getCurrentTime() < ($last + 3600)) {
      return [
        'values' => [],
      ];
    }

    $signature = $this->state->get("farm_jd.etag.$api_type", '');
    $changes = $this->jdClient->getOrganizationApiListChanges($api_type, $signature);
    $this->state->set("farm_jd.etag.$api_type.last_updated", $this->time->getCurrentTime());
    if (count($changes['values']) == 0) {
      return $changes;
    }

    $upsert = $this->database->upsert('farm_jd_updates_machines')
      ->fields(['jd_id', 'name', 'make', 'model'])
      ->key('jd_id');

    foreach ($changes['values'] as $machine) {
      $upsert->values([
        'jd_id' => $machine['id'],
        'name' => $machine['name'],
        'make' => $machine['equipmentMake']['name'] ?? '',
        'model' => $machine['equipmentModel']['name'] ?? '',
      ]);
    }
    $upsert->execute();

    if (!empty($changes['signature'])) {
      $this->state->set("farm_jd.etag.$api_type", $changes['signature']);
    }

    return $changes;
  }

  /**
   * {@inheritdoc}
   */
  public function checkFieldUpdates(bool $force = TRUE): array {
    $api_type = 'fields';

    // Bail if organization is not configured.
    if (!$this->jdClient->organizationIsConfigured()) {
      return [
        'values' => [],
      ];
    }

    // Check last update.
    if (!$force && ($last = $this->getApiLastUpdate($api_type)) && $this->time->getCurrentTime() < $last + (3600)) {
      return [
        'values' => [],
      ];
    }

    $signature = $this->state->get("farm_jd.etag.$api_type", '');
    $changes = $this->jdClient->getOrganizationApiListChanges($api_type, $signature);
    $this->state->set("farm_jd.etag.$api_type.last_updated", $this->time->getCurrentTime());
    if (count($changes['values']) == 0) {
      return $changes;
    }

    $upsert = $this->database->upsert('farm_jd_updates_fields')
      ->fields(['jd_id', 'name', 'modified'])
      ->key('jd_id');

    foreach ($changes['values'] as $field) {
      $upsert->values([
        'jd_id' => $field['id'],
        'name' => $field['name'],
        'modified' => strtotime($field['lastModifiedTime']),
      ]);
    }
    $upsert->execute();

    if (!empty($changes['signature'])) {
      $this->state->set("farm_jd.etag.$api_type", $changes['signature']);
    }
    return $changes;
  }

  /**
   * {@inheritdoc}
   */
  public function checkFieldOperationUpdates(bool $force = TRUE): array {

    // Bail if organization is not configured.
    if (!$this->jdClient->organizationIsConfigured()) {
      return [
        'values' => [],
      ];
    }

    // Check last update.
    if (!$force && ($last = $this->getApiLastUpdate('field_operations')) && $this->time->getCurrentTime() < $last + (3600)) {
      return [
        'values' => [],
      ];
    }

    // Query assets grouped by id_tag values.
    $asset_storage = $this->entityTypeManager->getStorage('asset');
    $asset_id_mapping = $asset_storage->getAggregateQuery()
      ->accessCheck(TRUE)
      ->condition('type', 'land')
      ->condition('id_tag.%delta.type', 'john_deere_id')
      ->condition('jd_data', NULL, 'IS NOT NULL')
      ->groupBy('name')
      ->groupBy('id_tag.id')
      ->groupBy('id_tag.type')
      ->groupBy('id')
      ->execute();

    // Map JD ids to asset IDs.
    $jd_field_ids = array_column($asset_id_mapping, 'id_tag_id');
    $field_names = array_column($asset_id_mapping, 'name');
    $mapping = array_combine($jd_field_ids, $field_names);
    $changes = ['values' => []];
    foreach ($mapping as $field_id => $field_name) {
      array_push($changes['values'], ...$this->checkFieldFieldOperationUpdates($field_name, $field_id)['values'] ?? []);
    }
    $this->state->set('farm_jd.etag.field_operations.last_updated', $this->time->getCurrentTime());
    return $changes;
  }

  /**
   * {@inheritdoc}
   */
  public function checkFieldFieldOperationUpdates(string $field_name, string $field_id): array {
    // Bail if organization is not configured.
    if (!$this->jdClient->organizationIsConfigured()) {
      return [
        'values' => [],
      ];
    }

    $signatures = $this->state->get('farm_jd.etag.field_operations', []);
    $signature = $signatures[$field_id] ?? '';
    $changes = $this->jdClient->getFieldFieldOperationsChanges($field_id, $signature);

    if (count($changes['values']) == 0) {
      return $changes;
    }

    // Keep track of field operation IDs.
    // Sometimes we get duplicate operation IDs which does not work with the
    // DB upsert on unique jd_id values.
    $to_insert = [];

    $upsert = $this->database->upsert('farm_jd_updates_field_operations')
      ->fields(['jd_id', 'field_name', 'field_id', 'operation_type', 'season', 'product', 'start', 'end', 'modified'])
      ->key('jd_id');

    foreach ($changes['values'] as $field) {

      // Queue the shape file for creation.
      if ($this->jdClient->getAutosyncShapefiles()) {
        $this->jdClient->checkFieldOperationShapefile($field['id']);
      }

      // Build product names from varieties and products.
      $product_names = [];
      foreach ($field['varieties'] ?? [] as $variety) {
        if (isset($variety['name'])) {
          $product_names[] = $variety['name'];
        }
      }
      foreach ($field['products'] ?? [] as $product) {
        if (isset($product['name'])) {
          $product_names[] = $product['name'];
        }
      }
      $product_name = implode(', ', $product_names);

      if (!in_array($field['id'], $to_insert)) {
        $to_insert[] = $field['id'];
        $upsert->values([
          'jd_id' => $field['id'],
          'field_name' => $field_name,
          'field_id' => $field_id,
          'season' => $field['cropSeason'] ?? '',
          'product' => $product_name,
          'operation_type' => $field['fieldOperationType'],
          'start' => strtotime($field['startDate']),
          'end' => strtotime($field['endDate']),
          'modified' => strtotime($field['modifiedTime']),
        ]);
      }
    }
    $upsert->execute();

    if (!empty($changes['signature'])) {
      $signatures[$field_id] = $changes['signature'];
      $this->state->set('farm_jd.etag.field_operations', $signatures);
    }
    return $changes;
  }

}
