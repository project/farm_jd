<?php

namespace Drupal\farm_jd;

/**
 * Interface for the JD sync service.
 */
interface JDSyncInterface {

  /**
   * Helper function to reset API updates.
   *
   * @param string $api_type
   *   The API to reset.
   */
  public function resetApiUpdates(string $api_type);

  /**
   * Helper function to get the last update time for an API.
   *
   * @param string $api_type
   *   The API to reset.
   *
   * @return int|null
   *   Last updated timestamp or NULL if never.
   */
  public function getApiLastUpdate(string $api_type): int|NULL;

  /**
   * Helper function to check equipment updates.
   *
   * @param bool $force
   *   Flag to force updates regardless of last check. Defaults to TRUE.
   *
   * @return array
   *   Array of equipment changes.
   */
  public function checkEquipmentUpdates(bool $force = TRUE): array;

  /**
   * Helper function to check field updates.
   *
   * @param bool $force
   *   Flag to force updates regardless of last check. Defaults to TRUE.
   *
   * @return array
   *   Array of field changes.
   */
  public function checkFieldUpdates(bool $force = TRUE): array;

  /**
   * Helper function to check field operation updates.
   *
   * @param bool $force
   *   Flag to force updates regardless of last check. Defaults to TRUE.
   *
   * @return array
   *   Array of field changes.
   */
  public function checkFieldOperationUpdates(bool $force = TRUE): array;

  /**
   * Helper function to check field operation updates for a single field.
   *
   * @return array
   *   Array of field changes.
   */
  public function checkFieldFieldOperationUpdates(string $field_name, string $field_id): array;

}
