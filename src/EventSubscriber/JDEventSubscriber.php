<?php

namespace Drupal\farm_jd\EventSubscriber;

use Drupal\asset\Entity\Asset;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\farm_jd\Event\JDEvent;
use Drupal\farm_jd\JDClientInterface;
use Drupal\farm_jd\JDSyncInterface;
use Drupal\file\FileRepositoryInterface;
use Drupal\log\Entity\Log;
use Drupal\quantity\Entity\Quantity;
use Drupal\taxonomy\Entity\Term;
use Drupal\token\TokenInterface;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Main event subscriber for JD data.
 */
class JDEventSubscriber implements EventSubscriberInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManger;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * The token service.
   *
   * @var \Drupal\token\TokenInterface
   */
  protected $token;

  /**
   * The John Deere client service.
   *
   * @var \Drupal\farm_jd\JDClientInterface
   */
  protected $jdClient;

  /**
   * The JD sync service.
   *
   * @var \Drupal\farm_jd\JDSyncInterface
   */
  protected $jdSync;

  /**
   * The file directory for JD.
   *
   * @var string
   */
  protected $fileDirectory;

  /**
   * Event subscriber constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository service.
   * @param \Drupal\farm_jd\JDClientInterface $jd_client
   *   The JD client service.
   * @param \Drupal\farm_jd\JDSyncInterface $jdSync
   *   The JD Sync service.
   */
  public function __construct(
    Connection $database,
    LoggerInterface $logger,
    EntityTypeManagerInterface $entity_type_manager,
    FileSystemInterface $file_system,
    FileRepositoryInterface $file_repository,
    TokenInterface $token,
    JDClientInterface $jd_client,
    JDSyncInterface $jdSync
  ) {
    $this->database = $database;
    $this->logger = $logger;
    $this->entityTypeManger = $entity_type_manager;
    $this->fileSystem = $file_system;
    $this->fileRepository = $file_repository;
    $this->token = $token;
    $this->jdClient = $jd_client;
    $this->jdSync = $jdSync;

    // Replace tokens to generate the file directory.
    $scheme = \Drupal::configFactory()->get('system.file')->get('default_scheme') ?? 'private';
    $directory_pattern = "$scheme://farm/log/[date:custom:Y]-[date:custom:m]";
    $this->fileDirectory = $this->token->replace($directory_pattern);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      JDEvent::FIELD => 'importField',
      JDEvent::MACHINE => 'importMachine',
      JDEvent::FIELD_OPERATION => 'importFieldOperation',
    ];
  }

  /**
   * Import machine.
   *
   * @param \Drupal\farm_jd\Event\JDEvent $event
   *   The JD event.
   */
  public function importMachine(JDEvent $event) {
    $response_data = $this->jdClient->getJdMachineById($event->id);
    if (empty($response_data)) {
      return;
    }

    // Query for existing equipment.
    $existing_equipment = $this->entityTypeManger->getStorage('asset')->loadByProperties([
      'type' => 'equipment',
      'id_tag.type' => 'john_deere_id',
      'id_tag.id' => $event->id,
    ]);
    $equipment = reset($existing_equipment);

    // Or create new equipment.
    if (empty($equipment)) {
      $equipment = Asset::create([
        'status' => 'active',
        'type' => 'equipment',
        'name' => $response_data['name'],
        'id_tag' => ['type' => 'john_deere_id', 'id' => $event->id],
      ]);
      $equipment->setNewRevision(TRUE);
      $equipment->setRevisionLogMessage(new TranslatableMarkup('Equipment imported from John Deere.'));
    }
    else {
      $equipment->setNewRevision(TRUE);
      $equipment->setRevisionLogMessage(new TranslatableMarkup('Equipment data updated from John Deere.'));
    }

    // Populate equipment values.
    $equipment
      ->set('manufacturer', $response_data['equipmentMake']['name'] ?? NULL)
      ->set('model', $response_data['equipmentModel']['name'] ?? NULL)
      ->set('serial_number', $response_data['vin'] ?? NULL)
      ->set('jd_data', Json::encode($response_data));
    $equipment->save();
    $event->assets[] = $equipment;

    // Remove from the updates table.
    $this->database->delete('farm_jd_updates_machines')
      ->condition('jd_id', $response_data['id'])
      ->execute();
  }

  /**
   * Import fields.
   *
   * @param \Drupal\farm_jd\Event\JDEvent $event
   *   The JD event.
   */
  public function importField(JDEvent $event) {
    $response_data = $this->jdClient->getJdFieldById($event->id);
    if (empty($response_data)) {
      return;
    }

    // Query for existing land.
    $existing_land = $this->entityTypeManger->getStorage('asset')->loadByProperties([
      'type' => 'land',
      'id_tag.type' => 'john_deere_id',
      'id_tag.id' => $event->id,
    ]);
    $land = reset($existing_land);

    // Or create new land.
    if (empty($land)) {
      $land = Asset::create([
        'type' => 'land',
        'land_type' => 'field',
        'name' => $response_data['name'],
        'id_tag' => ['type' => 'john_deere_id', 'id' => $event->id],
      ]);
      $land->setNewRevision(TRUE);
      $land->setRevisionLogMessage(new TranslatableMarkup('Field imported from John Deere.'));
    }
    else {
      $land->setNewRevision(TRUE);
      $land->setRevisionLogMessage(new TranslatableMarkup('Field data updated from John Deere.'));
    }

    // Extract geometry from the active boundary.
    $geometry = NULL;
    if (isset($response_data['boundaries']) && $active_boundary = reset($response_data['boundaries'])) {
      $response_data['_active_boundary'] = $active_boundary;
      $geometry = $this->extractGeometryFromBoundary($active_boundary);
    }

    $archived = $response_data['archived'] ?? FALSE;
    $land
      ->set('status', $archived ? 'archived' : 'active')
      ->set('intrinsic_geometry', $geometry)
      ->set('jd_data', Json::encode($response_data))
      ->save();
    $event->assets[] = $land;

    // Sync field operations for the new field.
    $this->jdSync->checkFieldFieldOperationUpdates($response_data['name'], $response_data['id']);

    // Remove from the updates table.
    $this->database->delete('farm_jd_updates_fields')
      ->condition('jd_id', $response_data['id'])
      ->execute();
  }

  /**
   * Import field operations.
   *
   * @param \Drupal\farm_jd\Event\JDEvent $event
   *   The JD event.
   */
  public function importFieldOperation(JDEvent $event) {
    $asset_storage = $this->entityTypeManger->getStorage('asset');

    // First request the field operation.
    $response_data = $this->jdClient->getFieldOperationById($event->id);

    // Values to populate for the log.
    $field_asset = NULL;
    $quantities = [];
    $material_quantities = [];

    // Extract additional data from the field operation links.
    $self_link = NULL;
    foreach ($response_data['links'] ?? [] as $link) {

      // Save the self link.
      if ($link['rel'] == 'self') {
        $self_link = $link['uri'];
      }

      // Field reference.
      if ($link['rel'] == 'field') {
        $parts = explode('/', rtrim($link['uri'], '/'));
        $field_id = end($parts);
        $land_assets = $asset_storage->loadByProperties([
          'type' => 'land',
          'id_tag.id' => $field_id,
          'id_tag.type' => 'john_deere_id',
        ]);
        $field_asset = reset($land_assets);
      }

      // Measurement types.
      if ($link['rel'] == 'measurementTypes') {
        $options[RequestOptions::HEADERS] = [
          'Accept-UOM-System' => $this->jdClient->getUnits(),
        ];
        $response = $this->jdClient->request(
          'GET',
          $link['uri'],
          $options,
        );
        if ($response->getStatusCode() != 200) {
          continue;
        }

        // Get measurements.
        $measurements = Json::decode($response->getBody());
        $response_data['_measurementTypes'] = $measurements;

        // Process each group of measurements.
        foreach ($measurements['values'] ?? [] as $measurement_group) {
          if (!isset($measurement_group['@type']) || $measurement_group['@type'] != 'FieldOperationMeasurement') {
            continue;
          }

          $category = $measurement_group['measurementCategory'];
          foreach ($measurement_group as $measurement_key => $measurement_value) {

            // Simplest case: process event measurements.
            if (is_array($measurement_value) && isset($measurement_value['@type']) && $measurement_value['@type'] == 'EventMeasurement') {
              $id = "{$measurement_key}_$category";
              $quantities[$id] = Quantity::create([
                'type' => 'standard',
                'label' => "$measurement_key $category ",
                'value' => $measurement_value['value'],
                'units' => $this->createOrLoadTerm($measurement_value['unitId'], 'unit'),
              ]);
              continue;
            }

            // Process nested variety and product totals.
            if (is_array($measurement_value)) {
              foreach ($measurement_value as $sub_measurements) {
                // ProductTotals were moved to be nested under ApplicationProductTotal.
                if (isset($sub_measurements['@type']) && $sub_measurements['@type'] == 'ApplicationProductTotal') {
                  foreach ($sub_measurements['productTotals'] ?? [] as $application_sub_measurement) {
                    if (isset($application_sub_measurement['@type']) && $application_sub_measurement['@type'] == 'ProductTotal') {
                      $product_name = $application_sub_measurement['name'];
                      foreach ($application_sub_measurement as $measurement_key => $sub_measurement_value) {
                        if (is_array($sub_measurement_value) && isset($sub_measurement_value['@type']) && $sub_measurement_value['@type'] == 'EventMeasurement') {
                          $id              = "{$product_name}_{$measurement_key}_$category";
                          $material_quantities[$id] = Quantity::create([
                            'type'          => 'material',
                            'label'         => "$measurement_key $category",
                            'value'         => $sub_measurement_value['value'],
                            'units'         => $this->createOrLoadTerm($sub_measurement_value['unitId'], 'unit'),
                            'material_type' => $this->createOrLoadTerm($product_name, 'material_type'),
                          ]);
                        }
                      }
                    }
                  }
                }

                // Process variety totals.
                if (isset($sub_measurements['@type']) && $sub_measurements['@type'] == 'VarietyTotal') {
                  $variety_name = $sub_measurements['name'];
                  foreach ($sub_measurements as $measurement_key => $sub_measurement_value) {
                    if (is_array($sub_measurement_value) && isset($sub_measurement_value['@type']) && $sub_measurement_value['@type'] == 'EventMeasurement') {
                      $id = "{$variety_name}_{$measurement_key}_$category";
                      $material_quantities[$id] = Quantity::create([
                        'type' => 'material',
                        'label' => "$measurement_key $category",
                        'value' => $sub_measurement_value['value'],
                        'units' => $this->createOrLoadTerm($sub_measurement_value['unitId'], 'unit'),
                        'material_type' => $this->createOrLoadTerm($variety_name, 'material_type'),
                      ]);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }

    // Sort quantities by keys to group target/results together.
    // Append material quantities.
    ksort($quantities);
    ksort($material_quantities);
    $all_quantities = array_merge(array_values($quantities), array_values($material_quantities));

    // Extract geometry from the field operation.
    $geometry = NULL;
    if ($self_link) {
      $boundary_response = $this->jdClient->request('GET', "$self_link/boundary");
      if ($boundary_response->getStatusCode() == 200 && $boundary = Json::decode($boundary_response->getBody())) {
        $geometry = $this->extractGeometryFromBoundary($boundary);
      }
    }

    // Determine log type from operation type.
    $operation_type = strtolower($response_data['fieldOperationType']);
    $log_type = 'activity';
    switch ($operation_type) {

      case 'application':
        $log_type = 'input';
        break;

      case 'seeding':
        $log_type = 'seeding';
        break;

      case 'harvest':
        $log_type = 'harvest';
        break;

      case 'tillage':
        $log_type = 'activity';
        break;
    }

    // Build product names from varieties and products.
    $product_names = [];
    foreach ($response_data['varieties'] ?? [] as $variety) {
      if (isset($variety['name'])) {
        $product_names[] = $variety['name'];
      }
    }
    foreach ($response_data['products'] ?? [] as $product) {
      if (isset($product['name'])) {
        $product_names[] = $product['name'];
      }
    }
    $product_name = implode(', ', $product_names);
    $operation_name = ucfirst($operation_type);
    $field_name = $field_asset ? $field_asset->label() : 'Unknown';
    $log_name = empty($product_name) ? "$operation_name - $field_name" : "$product_name: $operation_name - $field_name";
    $log = Log::create([
      'status' => 'done',
      'type' => $log_type,
      'name' => $log_name,
      'timestamp' => strtotime($response_data['startDate']),
      'location' => $field_asset,
      'geometry' => $geometry,
      'quantity' => $all_quantities,
      'jd_id' => $event->id,
      'jd_data' => Json::encode($response_data),
    ]);

    // Save the shape file.
    if ($this->jdClient->getAutosyncShapefiles() && $shape_file_data = $this->jdClient->getFieldOperationShapefile($event->id)) {
      try {
        $filename = "{$event->id}-Point-EachSection.zip";
        $destination = "$this->fileDirectory/$filename";
        $this->fileSystem->prepareDirectory($this->fileDirectory, FileSystemInterface::CREATE_DIRECTORY);
        $file = $this->fileRepository->writeData($shape_file_data, $destination);
        $log->get('file')->appendItem($file);
      }
      catch (\Exception $e) {
        $this->logger->error('Error saving shapefile to directory: ' . $e->getMessage());
      }
    }

    // Set revision info.
    $log->setNewRevision(TRUE);
    $log->setRevisionLogMessage(new TranslatableMarkup('Log imported from John Deere.'));

    // Save the log.
    $log->save();
    $event->logs[] = $log;

    // Remove from the updates table.
    $this->database->delete('farm_jd_updates_field_operations')
      ->condition('jd_id', $response_data['id'])
      ->execute();
  }

  /**
   * Helper function to extract geometry from a JD boundary.
   *
   * @param array $boundary
   *   The JD boundary.
   *
   * @return string
   *   The WKT geometry string.
   */
  protected function extractGeometryFromBoundary(array $boundary) {
    $geojson = [
      'type' => 'MultiPolygon',
      'coordinates' => [],
    ];
    foreach ($boundary['multipolygons'] ?? [] as $multipolygon) {
      foreach ($multipolygon['rings'] ?? [] as $ring) {
        $coordinates = array_map(function ($point) {
          return [floatval($point['lon']), floatval($point['lat'])];
        }, $ring['points'] ?? []);

        // @todo Ensure interior rings are added to correct exterior ring.
        // Each ring has an ID and parent ID but the parent ID always seems
        // to be 0 so not sure if the API represents these correctly.
        $ring_id = isset($ring['parentId']) ? $ring['parentId'] : $ring['id'];
        $geojson['coordinates'][$ring_id][] = $coordinates;
      }
    }
    return \geoPHP::load(Json::encode($geojson), 'geojson')->out('wkt');
  }

  /**
   * Given a term name, create or load a matching term entity.
   *
   * @param string $name
   *   The term name.
   * @param string $vocabulary
   *   The vocabulary to search or create in.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   The term entity that was created or loaded.
   */
  protected function createOrLoadTerm(string $name, string $vocabulary) {

    // First try to load an existing term.
    $search = $this->entityTypeManger->getStorage('taxonomy_term')->loadByProperties(['name' => $name, 'vid' => $vocabulary]);
    if (!empty($search)) {
      return reset($search);
    }

    // Start a new term entity with the provided values.
    /** @var \Drupal\taxonomy\TermInterface $term */
    $term = Term::create([
      'name' => $name,
      'vid' => $vocabulary,
    ]);
    $term->save();
    return $term;
  }

}
