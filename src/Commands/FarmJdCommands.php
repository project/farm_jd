<?php

namespace Drupal\farm_jd\Commands;

use Drupal\farm_jd\JDStatusTrait;
use Drush\Commands\DrushCommands;

/**
 * JD Drush Commands.
 */
class FarmJdCommands extends DrushCommands {

  use JDStatusTrait;

  /**
   * Check John Deere equipment updates.
   *
   * @command farm_jd:check_equipment_updates
   */
  public function checkEquipmentUpdates() {
    if ($this->checkJdStatus()) {
      $changes = $this->getJdSync()->checkEquipmentUpdates();
      $count = count($changes['values'] ?? []);
      $message = "Synced $count equipment updates.";
      $this->logger()->info($message);
      // phpcs:ignore
      \Drupal::logger('farm_jd')->info($message);
    }
  }

  /**
   * Check John Deere field updates.
   *
   * @command farm_jd:check_field_updates
   */
  public function checkFieldUpdates() {
    if ($this->checkJdStatus()) {
      $changes = $this->getJdSync()->checkFieldUpdates();
      $count = count($changes['values'] ?? []);
      $message = "Synced $count field updates.";
      $this->logger()->info($message);
      // phpcs:ignore
      \Drupal::logger('farm_jd')->info($message);
    }
  }

  /**
   * Check John Deere field operation updates.
   *
   * @command farm_jd:check_field_operation_updates
   */
  public function checkFieldOperationUpdates() {
    if ($this->checkJdStatus()) {
      $changes = $this->getJdSync()->checkFieldOperationUpdates();
      $count = count($changes['values'] ?? []);
      $message = "Synced $count field operation updates.";
      $this->logger()->info($message);
      // phpcs:ignore
      \Drupal::logger('farm_jd')->info($message);
    }
  }

  /**
   * Getter for the JD Sync service.
   *
   * @return \Drupal\farm_jd\JDSyncInterface
   *   The JD Sync service.
   */
  protected function getJdSync() {
    // phpcs:ignore
    return \Drupal::service('farm_jd.jd_sync');
  }

}
