<?php

namespace Drupal\farm_jd\Controller;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\farm_jd\JDClientInterface;
use Drupal\farm_jd\JDStatusTrait;
use Drupal\farm_jd\JDSyncInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for authorizing and connecting with John Deere.
 */
class AuthController extends ControllerBase {

  use JDStatusTrait;

  /**
   * The logger service.
   *
   * @var LoggerInterface
   */
  protected $logger;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The JDClient.
   *
   * @var \Drupal\farm_jd\JDClientInterface
   */
  protected $jdClient;

  /**
   * The JD Sync service.
   *
   * @var \Drupal\farm_jd\JDSyncInterface
   */
  protected $jdSync;

  /**
   * Constructs the BoundariesController.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\farm_jd\JDClientInterface $jd_client
   *   The JDClient.
   * @param \Drupal\farm_jd\JDSyncInterface $jd_sync
   *   The JD Sync service.
   */
  public function __construct(LoggerInterface $logger, TimeInterface $time, EntityTypeManagerInterface $entity_type_manager, JDClientInterface $jd_client, JDSyncInterface $jd_sync) {
    $this->logger = $logger;
    $this->time = $time;
    $this->entityTypeManager = $entity_type_manager;
    $this->jdClient = $jd_client;
    $this->jdSync = $jd_sync;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('logger.channel.farm_jd'),
          $container->get('datetime.time'),
          $container->get('entity_type.manager'),
          $container->get('farm_jd.jd_client'),
          $container->get('farm_jd.jd_sync'),
      );
  }

  /**
   * Provides the status of the John Deere account connection.
   *
   * @return array
   *   An array of renderable elements to display the status.
   */
  public function status(): array {
    $render = [];

    // Check the client.
    if (!$this->checkJdClient(TRUE)) {
      $settings = Link::createFromRoute($this->t('Configure the John Deere client.'), 'farm_jd.settings')->toRenderable();
      $render['message'] = [
        '#type' => 'container',
        'settings' => $settings,
      ];
      return $render;
    }

    // Create a link to the connection page and open it in a modal.
    $connect = Link::createFromRoute($this->t('Connect John Deere'), 'farm_jd.connect')->toRenderable();
    $connect['#attributes']['class'][] = 'use-ajax';
    $connect['#attributes']['data-dialog-type'] = 'modal';
    $connect['#attributes']['data-dialog-options'] = '{"height": "auto", "width": "500px"}';
    $connect['#attributes']['class'][] = 'button';
    $connect['#attributes']['class'][] = 'button--primary';

    // Check the organization.
    if (!$this->checkJdOrganization()) {
      $render['message'] = [
        '#type' => 'container',
        'message' => [
          '#markup' => $this->t('No John Deere organization connected. Connect a John Deere organization.'),
        ],
      ];

      $render['connect'] = $connect;
      return $render;
    }

    // Get the organization data from the JD Client.
    $org = $this->jdClient->getOrganization();

    // Display message and link to connect John Deere account if not connected.
    if (empty($org)) {
      $render['message'] = [
        '#type' => 'container',
        'message' => [
          '#markup' => $this->t('Failed to request John Deere organization. Please try again and reset the connection if the issue persists.'),
        ],
      ];

      $render['connect'] = $connect;

      $this->logger->error('Failed to request John Deere organization.');
      return $render;
    }
    else {
      // Render reset button if John Deere account is connected.
      $render['revoke'] = Link::createFromRoute('Reset John Deere connection', 'farm_jd.revoke')->toRenderable();
      $render['revoke']['#weight'] = 100;
      $render['revoke']['#attributes']['class'][] = 'button';
      $render['revoke']['#attributes']['class'][] = 'button--danger';
    }

    // Get the links from the organization data.
    $links = $org["links"] ?? NULL;

    if (!$links) {
      // Display a warning if links are undefined.
      $this->messenger()->addWarning('Failed to request John Deere organization. Please try again and reset the connection if the issue persists.');
      $this->logger->error('Failed to request John Deere organization. No links found.');
    }
    else {
      // Check for specific links and render appropriate messages and buttons.
      foreach ($links as $link) {
        // User without organization connection.
        if (isset($link['rel']) && $link['rel'] == 'connections') {
          $render['message'] = [
            '#type' => 'container',
            'message' => [
              '#markup' => $this->t("farmOS does not have access to your John Deere organization. Please add farmOS to your organization's connections in John Deere Ops Center."),
            ],
          ];

          // Create a link to add the connection.
          $redirectURI = Url::fromRoute('farm_jd.status')->setAbsolute(TRUE)->toString();
          $link = Link::fromTextAndUrl($this->t('Add Connection'), Url::fromUri($link['uri'], ['query' => ['redirect_uri' => $redirectURI]]))->toRenderable();
          $link['#attributes']['class'][] = 'button';
          $link['#attributes']['class'][] = 'button--primary';
          $render['access_link'] = $link;

          // Stop the loop since the appropriate message is rendered.
          break;
        }

        // User with organization connection.
        if (isset($link['rel']) && $link['rel'] == 'manage_connection') {
          $render['message'] = [
            '#type' => 'container',
            'message' => [
              '#markup' => $this->t('Connected to John Deere organization: @org_name', ['@org_name' => $org['name']]),
            ],
          ];

          // Create a link to manage the connection (open in a new tab).
          $link = Link::fromTextAndUrl($this->t('Manage Connections'), Url::fromUri($link['uri']))->toRenderable();
          $link['#attributes']['class'][] = 'button';
          $link['#attributes']['target'] = '_blank';
          $render['access_link'] = $link;

          // Stop the loop since the appropriate message is rendered.
          break;
        }
      }
    }

    return $render;
  }

  /**
   * Route to revoke JD access.
   */
  public function revoke() {

    // Reset state.
    $this->state()->delete('farm_jd.token');
    $this->state()->delete('farm_jd.org_id');

    // Reset API updates.
    $this->jdSync->resetApiUpdates('fields');
    $this->jdSync->resetApiUpdates('machines');
    $this->jdSync->resetApiUpdates('field_operations');

    $this->messenger()->addMessage($this->t('The John Deere connection was reset.'));
    $this->logger->info('John Deere connection was reset.');
    return new RedirectResponse(Url::fromRoute('farm_jd.status')->toString());
  }

  /**
   * Connect page.
   */
  public function connect(): array {
    // Render a description.
    $render['description'] = [
      '#type' => 'container',
      'text' => [
        '#markup' => $this->t('Click connect to login to John Deere and authorize access for farmOS. Once authorized you will be redirected back to farmOS to complete the connection.'),
      ],
    ];

    // Build authorization link.
    $jd_settings = $this->config('farm_jd.settings');
    $options = [
      'query' => [
        'response_type' => 'code',
        'client_id' => $jd_settings->get('client_id'),
        'client_secret' => $jd_settings->get('client_secret'),
        'state' => $this->getAuthorizationState(),
        'scope' => 'org1 ag1 ag2 eq1 work1 files offline_access',
        'redirect_uri' => $this->redirectUri(),
      ],
    ];
    $url = Url::fromUri($this->jdClient->getAuthUrl(), $options);
    $link = Link::fromTextAndUrl($this->t('Connect'), $url);

    // Render authorization link.
    $render['connect'] = $link->toRenderable();
    $render['connect']['#attributes'] = [
      'class' => ['button', 'button-action', 'button-primary', 'button-small'],
    ];

    return $render;
  }

  /**
   * Helper function that returns the OAuth redirect URI.
   *
   * @return string
   *   The redirect URI
   */
  protected function redirectUri(): string {
    $grantUrl = Url::fromRoute('farm_jd.grant');
    $url = $grantUrl->setAbsolute(TRUE)->toString();

    return $url;
  }

  /**
   * Grant page to complete the authorization flow.
   */
  public function grant(Request $request) {
    $render = [];

    $code = $request->get('code');
    if (empty($code)) {
      $render['error'] = [
        '#markup' => $this->t('No code provided. Please connect again.'),
      ];
      $this->logger->error('No authorization code received in Authorization code flow.');
      return $render;
    }

    $this->logger->info('Authorization code received.');

    // Complete the authorization flow.
    $jdSettings = $this->config('farm_jd.settings');
    $params = [
      'grant_type' => 'authorization_code',
      'client_id' => $jdSettings->get('client_id'),
      'client_secret' => $jdSettings->get('client_secret'),
      'code' => $code,
      'redirect_uri' => $this->redirectUri(),
    ];
    $token = $this->jdClient->grant($params);
    $this->clearAuthorizationState();

    // Check for a valid token.
    if (empty($token)) {
      $this->messenger()->addError($this->t('Connection failed. Please try again.'));
      $this->logger->error('Failed to retrieve an OAuth token for Authorization code flow.');
      return new RedirectResponse(Url::fromRoute('farm_jd.status')->toString());
    }

    $this->logger->info('Completed Authorization code flow.');
    $this->jdClient->setToken($token);

    // Connect the Organization.
    // The John Deere server takes a second to propagate the token to other
    // API servers and services.
    sleep(2);
    $response = $this->jdClient->request('GET', 'organizations');

    // Check for failure and retry.
    if ($response->getStatusCode() !== 200) {
      $response = $this->jdClient->request('GET', 'organizations');
    }

    // Check for failure and log message.
    if ($response->getStatusCode() !== 200) {
      $response_body = Json::decode($response->getBody());
      $error = Json::encode($response_body, JSON_PRETTY_PRINT);
      $this->logger->error("Could not retrieve John Deere account: $error");
    }
    else {
      // Return the first account.
      $response_body = Json::decode($response->getBody());
      if (isset($response_body['values']) && count($response_body['values'])) {
        return $this->organizationSelections($response_body['values']);
      }
      else {
        $this->messenger()->addError($this->t('John connection failed. Could not connect account. Please try again.'));
      }
    }

    return new RedirectResponse(Url::fromRoute('farm_jd.status')->toString());
  }

  /**
   * Helper function that returns current authorization state.
   *
   * @return string
   *   The authorization state
   */
  protected function getAuthorizationState(): string {
    $state = $this->state()->get('farm_jd.authorization_state');
    if (empty($state)) {
      $state = bin2hex(random_bytes(16));
      $this->state()->set('farm_jd.authorization_state', $state);
    }

    return $state;
  }

  /**
   * Helper function to clear the authorization state.
   */
  protected function clearAuthorizationState() {
    $this->state()->delete('farm_jd.authorization_state');
  }

  /**
   * Formats organizations into select options.
   *
   * @param array $orgs
   *   The organizations array.
   *
   * @return array|null
   *   The formatted options array with IDs as keys and names as values.
   */
  protected function formatOrgOptions(array $orgs) {

    $options = [];

    foreach ($orgs as $org) {
      if (isset($org['@type']) && $org['@type'] === 'Organization') {
        $options[$org['id']] = $org['name'];
      }
    }

    return $options;
  }

  /**
   * Redirects to the organization selection route with formatted organizations.
   *
   * @param array $orgs
   *   The organizations array.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirect response to the organization selection route.
   */
  protected function organizationSelections(array $orgs): RedirectResponse {
    $orgOptions = $this->formatOrgOptions($orgs);

    // Set the data in Drupal state.
    $this->state()->set('farm_jd.org_options', $orgOptions);

    return new RedirectResponse(Url::fromRoute('farm_jd.organization')->toString());
  }

}
