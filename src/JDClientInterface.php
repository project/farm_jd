<?php

namespace Drupal\farm_jd;

use GuzzleHttp\ClientInterface;

/**
 * Interface for John Deere client.
 */
interface JDClientInterface extends ClientInterface {

  /**
   * Helper function to return if the JD client is configured.
   *
   * @return bool
   *   Boolean indicating if the client is configured.
   */
  public function clientIsConfigured(): bool;

  /**
   * Helper function to return if the JD organization is configured.
   *
   * @return bool
   *   Boolean indicating if the organization is configured.
   */
  public function organizationIsConfigured(): bool;

  /**
   * Helper function to return the OAuth Authorization URL.
   *
   * @return string
   *   The OAuth Auth URL.
   */
  public function getAuthUrl();

  /**
   * Helper function to return the OAuth token URL.
   *
   * @return string
   *   The OAuth token URL.
   */
  public function getTokenUrl();

  /**
   * Helper function to return the configured JD units.
   *
   * @return string|null
   */
  public function getUnits(): ?string;

  /**
   * Returns if autosyncing shapefiles is enabled.
   *
   * @return bool
   */
  public function getAutosyncShapefiles(): bool;

  /**
   * Helper function to get the connected organzation.
   *
   * @return array|null
   *   The organzation data.
   */
  public function getOrganization();

  /**
   * Helper function to set the John Deere OAuth token.
   *
   * @param array $token
   *   The OAuth token array.
   */
  public function setToken(array $token);

  /**
   * Helper function to perform OAuth grants.
   *
   * @param array $params
   *   Params for the OAuth grant.
   *
   * @return array
   *   The new token.
   */
  public function grant(array $params): array;

  /**
   * Helper function to refresh the JD OAuth token.
   *
   * @return array
   *   The new token.
   */
  public function refreshToken(): array;

  /**
   * Request an organization API list.
   *
   * @param string $api_type
   *   The API type.
   * @param array $options
   *   Request options.
   *
   * @return \Psr\Http\Message\ResponseInterface|null
   *   The response object or NULL.
   */
  public function requestOrganizationApiList(string $api_type, array $options);

  /**
   * Fetches all pages of API data for the connected organization.
   *
   * @param string $api_type
   *   The type of API endpoint to call.
   * @param array $options
   *   Any request options.
   *
   * @return array|null
   *   Returns the decoded response body or NULL on failure.
   */
  public function getOrganizationApiList(string $api_type, array $options = []);

  /**
   * Get changes in an organization API list using JD eTags.
   *
   * @param string $api_type
   *   The API type.
   * @param string $signature
   *   The eTag signature.
   *
   * @return array
   *   Array of changed values.
   */
  public function getOrganizationApiListChanges(string $api_type, string $signature = '');

  /**
   * Fetches details of a specific machine based on its ID.
   *
   * @param string $machine_id
   *   The ID of the machine.
   *
   * @return array|null
   *   Returns the decoded response body or NULL on failure.
   */
  public function getJdMachineById(string $machine_id);

  /**
   * Fetches details of a specific field based on its ID.
   *
   * @param string $field_id
   *   The ID of the field.
   *
   * @return array|null
   *   Returns the decoded response body or NULL on failure.
   */
  public function getJdFieldById(string $field_id);

  /**
   * Fetches details of a specific field operation.
   *
   * @param string $field_operation_id
   *   The ID of the field operation.
   *
   * @return array|null
   *   Returns the decoded response body or NULL on failure.
   */
  public function getFieldOperationById(string $field_operation_id);

  /**
   * Helper function to return field operation request promises.
   *
   * @param string $field_id
   *   The JD field ID.
   * @param array $options
   *   The request options.
   *
   * @return \GuzzleHttp\Promise\PromiseInterface
   *   The request promise.
   */
  public function requestFieldFieldOperations(string $field_id, array $options);

  /**
   * Get field operations changes using JD eTags.
   *
   * @param string $field_id
   *   The field ID.
   * @param string $signature
   *   The eTag signature.
   *
   * @return array
   *   The field operation changes.
   */
  public function getFieldFieldOperationsChanges(string $field_id, string $signature = '');

  /**
   * Gets the field operation shapefile.
   *
   * @param string $operation_id
   *   The field operation ID.
   *
   * @return mixed
   */
  public function getFieldOperationShapefile(string $operation_id);

  /**
   * Checks if a field operation is available or queues for processing.
   *
   * @param string $operation_id
   *   The field operation ID.
   *
   * @return mixed
   */
  public function checkFieldOperationShapefile(string $operation_id);

}
