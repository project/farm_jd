<?php

namespace Drupal\farm_jd;

use Drupal\user\RoleInterface;

/**
 * Add permissions to the Account Admin role.
 */
class AccountAdminPermissions {

  /**
   * Add permissions to default farmOS roles.
   *
   * @param \Drupal\user\RoleInterface $role
   *   The role to add permissions to.
   *
   * @return array
   *   An array of permission strings.
   */
  public function permissions(RoleInterface $role) {
    $perms = [];

    // Add permissions to the farm_account_admin role.
    if ($role->id() == 'farm_account_admin') {
      $perms[] = 'administer farm_jd';
      $perms[] = 'connect farm_jd';
    }

    return $perms;
  }

}
