<?php

namespace Drupal\farm_jd;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;

/**
 * Factory service for John Deere Client.
 */
class JDClientFactory {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructor for the JDClientFactory.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StateInterface $state, TimeInterface $time) {
    $this->configFactory = $config_factory;
    $this->state = $state;
    $this->time = $time;
  }

  /**
   * Returns a JDClient.
   *
   * @return \Drupal\farm_jd\JDClient
   *   The JDClient.
   */
  public function getApiClient() {

    // Units.
    $configured_units = $this->configFactory->get('quantity.settings')->get('system_of_measurement');
    $jd_units = $configured_units === 'metric' ? 'METRIC' : 'ENGLISH';

    // Create client and set the current token.
    $jd_config = $this->configFactory->get('farm_jd.settings');
    $clientConfig = [
      'client_id' => $jd_config->get('client_id'),
      'client_secret' => $jd_config->get('client_secret'),
      'environment' => $jd_config->get('environment'),
      'autosync_shapefiles' => $jd_config->get('autosync_shapefiles'),
      'org_id' => $this->state->get('farm_jd.org_id'),
      'units' =>  $jd_units
    ];

    $client = new JDClient($this->configFactory, $this->state, $this->time, $clientConfig);

    // Ensure a default empty array if token is missing.
    $client->setToken($this->state->get('farm_jd.token', []));

    return $client;
  }

}
