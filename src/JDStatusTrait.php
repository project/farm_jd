<?php

namespace Drupal\farm_jd;

use Drupal\Core\Messenger\MessengerTrait;

/**
 * Trait for checking the JD status.
 */
trait JDStatusTrait {

  use MessengerTrait;

  /**
   * Helper function to check all JD status.
   *
   * @param bool $message
   *   Boolean to log a warning message.
   *
   * @return bool
   *   Boolean indicating JD status.
   */
  protected function checkJdStatus(bool $message = FALSE): bool {

    // Check client configuration and bail if not set.
    if (!$this->checkJdClient($message)) {
      return FALSE;
    }

    // Finally check the organization configuration.
    return $this->checkJdOrganization($message);
  }

  /**
   * Helper function to check the client status.
   *
   * @param bool $message
   *   Boolean to log a warning message.
   *
   * @return bool
   *   Boolean indicating JD client status.
   */
  protected function checkJdClient(bool $message = FALSE) {
    $configured = $this->getJdClient()->clientIsConfigured();
    if (!$configured && $message) {
      $this->messenger()->addWarning($this->t('The John Deere client is not configured.'));
    }
    return $configured;
  }

  /**
   * Helper function to check the organization status.
   *
   * @param bool $message
   *   Boolean to log a warning message.
   *
   * @return bool
   *   Boolean indicating JD organization status.
   */
  protected function checkJdOrganization(bool $message = FALSE) {
    $configured = $this->getJdClient()->organizationIsConfigured();
    if (!$configured && $message) {
      $this->messenger()->addWarning($this->t('The John Deere organization is not configured.'));
    }
    return $configured;
  }

  /**
   * Helper function to get a JD client.
   *
   * @return \Drupal\farm_jd\JDClientInterface
   *   The JD client.
   */
  protected function getJdClient(): JDClientInterface {
    if (!isset($this->jdClient)) {
      $this->jdClient = \Drupal::service('farm_jd.jd_client');
    }
    return $this->jdClient;
  }

}
