<?php

namespace Drupal\farm_jd\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event that is fired when creating or updating JD events.
 */
class JDEvent extends Event {

  // Event types.
  const FIELD = 'field';
  const MACHINE = 'machine';
  const FIELD_OPERATION = 'field_operation';

  // Action types.
  const ACTION_CREATED = 'created';
  const ACTION_UPDATED = 'updated';
  const ACTION_DELETED = 'deleted';
  const ACTION_IMPORTED = 'import';

  /**
   * The ID of the event object.
   *
   * @var string
   */
  public string $id;

  /**
   * An string identifying the action for the event.
   *
   * Should be created, deleted, modified or imported.
   *
   * @var string
   */
  public string $action;

  /**
   * Array of event data.
   *
   * @var array
   */
  public array $eventData;

  /**
   * Array of assets created from the event.
   *
   * @var \Drupal\asset\Entity\AssetInterface[]
   */
  public array $assets;

  /**
   * Array of logs created from the event.
   *
   * @var \Drupal\log\Entity\LogInterface[]
   */
  public array $logs;

  /**
   * Creates the event object.
   *
   * @param string $id
   *   The object ID.
   * @param string $action
   *   The event action.
   * @param array $eventData
   *   The event data.
   */
  public function __construct(string $id, string $action, array $eventData) {
    $this->id = $id;
    $this->action = $action;
    $this->eventData = $eventData;
    $this->assets = [];
    $this->logs = [];
  }

}
